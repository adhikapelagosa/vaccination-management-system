
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as anonymous module.
        define('IndonesianDistricts', [], factory);
    } else {
        // Browser globals.
        factory();
    }
})(function () {

    var IndonesianDistricts = {
            45: {
                'A-J': [
                    {code: '110000', address: 'Aceh (NAD)'},
                    {code: '120000', address: 'Bali'},
                    {code: '130000', address: 'Banten'},
                    {code: '140000', address: 'Bengkulu'},
                    {code: '150000', address: 'Gorontalo'},
                    {code: '160000', address: 'DKI Jakarta'},
                    {code: '170000', address: 'Jambi'},
                    {code: '180000', address: 'Jawa Barat'},
                    {code: '190000', address: 'Jawa Tengah'},
                    {code: '210000', address: 'Jawa Timur'}],
                'K-L': [
                    {code: '220000', address: 'Kalimantan Barat'},
                    {code: '230000', address: 'Kalimantan Selatan'},
                    {code: '240000', address: 'Kalimantan Tengah'},
                    {code: '250000', address: 'Kalimantan Timur'},
                    {code: '260000', address: 'Kalimantan Utara'},
                    {code: '270000', address: 'Kepulauan Bangka Belitung'},
                    {code: '280000', address: 'Kepulauan Riau'},
                    {code: '290000', address: 'Lampung'}],
                'M-R': [
                    {code: '310000', address: 'Maluku'},
                    {code: '320000', address: 'Maluku Utara'},
                    {code: '330000', address: 'Nusa Tenggara Barat'},
                    {code: '340000', address: 'Nusa Tenggara Timur'},
                    {code: '350000', address: 'Papua'},
                    {code: '360000', address: 'Papua Barat'},
                    {code: '370000', address: 'Riau'}],
                'S-Y': [
                    {code: '380000', address: 'Sulawesi Barat'},
                    {code: '390000', address: 'Sulawesi Selatan'},
                    {code: '410000', address: 'Sulawesi Tengah'},
                    {code: '420000', address: 'Sulawesi Tenggara'},
                    {code: '430000', address: 'Sulawesi Utara'},
                    {code: '440000', address: 'Sumatera Barat'},
                    {code: '450000', address: 'Sumatera Selatan'},
                    {code: '460000', address: 'Sumatera Utara'},
                    {code: '470000', address: 'DI Yogyakarta'}],
            },
            110000: {
                111000: 'Aceh Tengah',
                112000: 'Banda Aceh',
            },
            111000: {
                111100: 'Takengon',
                111200: 'Linge',
            },
            112000: {
                112100: 'Lueng Bata',
                112200: 'Kuta Alam',
            }
        }
        ;

    if (typeof window !== 'undefined') {
        window.IndonesianDistricts = IndonesianDistricts;
    }

    return IndonesianDistricts;

});


